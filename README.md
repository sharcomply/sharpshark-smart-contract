# How to deploy

1. Fill `.env`
2. Run `npm i`
3. (if deployed before) Delete `build` dir
4. Run `ganache-cli` to calculate gas and price
5. Fill that values to `truffle-config.js`
6. Run `npx truffle console --network test`
7. Run `migrate`
8. Verify contract `truffle run verify SharkKeeper --network test`