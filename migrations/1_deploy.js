// migrations/2_deploy.js
// SPDX-License-Identifier: MIT
const artifact = artifacts.require("SharkKeeper");

module.exports = function (deployer) {
  deployer.deploy(artifact, "SharkKeeperTest", "SKT", "0x075328c0dedb4280784fb60be5818b5ee397bbc6", "https://ipfs.io/ipfs/");
  // deployer.deploy(artifact, "Test1", "Test1", "https://dev.newfoundtreasure.io/api/v1/public/token-meta-info/", "0x87270c3c0E23322cc77ae17Aa62cb042E17f89C6");
};
