require('dotenv').config();
const HDWalletProvider = require('@truffle/hdwallet-provider');


module.exports = {
  networks: {
    ethTest: {
      provider: () => new HDWalletProvider(process.env.MNEMONIC, process.env.ETHEREUM_WS_URL),
      network_id: 4,       // Rinkeby's id
      gas: 6721975,
      gasPrice: 20000000000,  // 1 gwei (in wei) (default: 100 gwei)
      from: '0x82aAa60CE709a7C1cEd66B2Ca8016E3FE0cd3A8f',
      timeoutBlocks: 50000,
      skipDryRun: true,
      // websocket: true
    },
    bscTest: {
      provider: () => new HDWalletProvider(process.env.MNEMONIC, process.env.BSC_WS_URL),
      network_id: 97,
      gas: 6721975,
      gasPrice: 20000000000,  // 1 gwei (in wei) (default: 100 gwei)
      from: '0x82aAa60CE709a7C1cEd66B2Ca8016E3FE0cd3A8f',
      timeoutBlocks: 50000,
      skipDryRun: true,
      // websocket: true
    },
    polygonTest: {
      provider: () => new HDWalletProvider(process.env.MNEMONIC, process.env.POLYGON_WS_URL),
      network_id: 80001,
      gas: 6721975,
      gasPrice: 50000000000,  // 1 gwei (in wei) (default: 100 gwei)
      from: '0x82aAa60CE709a7C1cEd66B2Ca8016E3FE0cd3A8f',
      timeoutBlocks: 50000,
      skipDryRun: true,
      // websocket: true
    },
  },

  mocha: {},

  compilers: {
    solc: {
      version: "0.8.10",
      settings: {
        optimizer: {
          enabled: true,
          runs: 1000
        },
      }
    }
  },
  plugins: ['truffle-plugin-verify'],
  api_keys: {
    etherscan: process.env.ETHEREUM_SCAN_KEY,
    bscscan: process.env.BSC_SCAN_KEY,
    polygonscan: process.env.POLYGON_SCAN_KEY,
  }
};
